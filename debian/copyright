Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Jai-core
Source: http://java.net/projects/jai-core

Files: *
Copyright: © 2005 Sun Microsystems, Inc. All rights reserved.
License: JRL Version 1.5
 I. DEFINITIONS.
 .
 "Licensee " means You and any other party that has entered into and has
 in effect a version of this License.
 .
 "Modifications" means any (a) change or addition to the Technology or
 (b) new source or object code implementing any portion of the
 Technology.
 .
 "Sun" means Sun Microsystems, Inc. and its successors and assignees.
 .
 "Research Use" means research, evaluation, or development for the
 purpose of advancing knowledge, teaching, learning, or customizing the
 Technology or Modifications for personal use. Research Use expressly
 excludes use or distribution for direct or indirect commercial
 (including strategic) gain or advantage.
 .
 "Technology" means the source code, object code and specifications of
 the technology made available by Sun pursuant to this License.
 .
 "Technology Site" means the website designated by Sun for accessing the
 Technology. 
 .
 "You" means the individual executing this License or the legal entity or
 entities represented by the individual executing this License.
 .
 II. PURPOSE.
 .
 Sun is licensing the Technology under this Java Research License (the
 "License") to promote research, education, innovation, and development
 using the Technology.
 .
 COMMERCIAL USE AND DISTRIBUTION OF TECHNOLOGY AND MODIFICATIONS IS
 PERMITTED ONLY UNDER A SUN COMMERCIAL LICENSE.
 .
 III. RESEARCH USE RIGHTS.
 .
 A. License Grant. Subject to the conditions contained herein, Sun 
 grants to You a non-exclusive, non-transferable, worldwide, and
 royalty-free license to do the following for Your Research Use only:  
 .
 1. Reproduce, create Modifications of,  and use the Technology alone, or
 with Modifications; 
 .
 2. Share source code of the Technology alone, or with Modifications,
 with other Licensees; and 
 .
 3. Distribute object code of the Technology, alone, or with
 Modifications, to any third parties for Research Use only, under a
 license of Your choice that is consistent with this License; and publish
 papers and books discussing the Technology which may include relevant
 excerpts that do not in the aggregate constitute a significant portion
 of the Technology.
 .
 B. Residual Rights. You may use any information in intangible form that
 you remember after accessing the Technology, except when such use
 violates Sun's copyrights or patent rights.
 .
 C. No Implied Licenses.  Other than the rights granted herein, Sun
 retains all rights, title, and interest in Technology, and You retain
 all rights, title, and interest in Your Modifications and associated
 specifications, subject to the terms of this License.
 .
 D. Open Source Licenses.  Portions of the Technology may be provided
 with notices and open source licenses from open source communities and
 third parties that govern the use of those portions, and any licenses
 granted hereunder do not alter any rights and obligations you may have
 under such open source licenses, however, the disclaimer of warranty and
 limitation of liability provisions in this License will apply to all
 Technology in this distribution.
 .
 IV. INTELLECTUAL PROPERTY REQUIREMENTS
 .
 As a condition to Your License, You agree to comply with the following
 restrictions and responsibilities:
 .
 A. License and Copyright Notices. You must include a copy of this Java
 Research License in a Readme file for any Technology or Modifications
 you distribute. You must also include the following statement, "Use and
 distribution of this technology is subject to the Java Research License
 included herein", (a) once prominently in the source code tree and/or
 specifications for Your source code distributions, and (b) once in the
 same file as Your copyright or proprietary notices for Your binary code
 distributions. You must cause any files containing Your Modification to
 carry prominent notice stating that You changed the files. You must not
 remove or alter any copyright or other proprietary notices in the
 Technology.
 .
 B. Licensee Exchanges.    Any Technology and Modifications You receive
 from any Licensee are governed by this License. 
 .
 V. GENERAL TERMS.
 .
 A. Disclaimer Of Warranties.
 .
 THE TECHNOLOGY IS PROVIDED "AS IS", WITHOUT WARRANTIES OF ANY KIND,
 EITHER EXPRESS OR IMPLIED INCLUDING, WITHOUT LIMITATION, WARRANTIES THAT
 THE TECHNOLOGY IS FREE OF DEFECTS, MERCHANTABLE, FIT FOR A PARTICULAR
 PURPOSE, OR NON-INFRINGING OF THIRD PARTY RIGHTS.  YOU AGREE THAT YOU
 BEAR THE ENTIRE RISK IN CONNECTION WITH YOUR USE AND DISTRIBUTION OF ANY
 AND ALL TECHNOLOGY  UNDER THIS LICENSE.
 .
 B. Infringement; Limitation Of Liability.
 .
 1. If any portion of, or functionality implemented by, the Technology
 becomes the subject of a claim or threatened claim of infringement
 ("Affected Materials"), Sun may, in its unrestricted discretion, suspend
 Your rights to use and distribute the Affected Materials under this
 License. Such suspension of rights will be effective immediately upon
 Sun's posting of notice of suspension on the Technology Site.
 .
 2. IN NO EVENT WILL SUN BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE,
 SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES IN CONNECTION WITH OR
 ARISING OUT OF THIS LICENSE (INCLUDING, WITHOUT LIMITATION, LOSS OF
 PROFITS, USE, DATA, OR ECONOMIC ADVANTAGE OF ANY SORT), HOWEVER IT
 ARISES AND ON ANY THEORY OF LIABILITY (including negligence), WHETHER OR
 NOT SUN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. LIABILITY
 UNDER THIS SECTION V.B.2 SHALL BE SO LIMITED AND EXCLUDED,
 NOTWITHSTANDING FAILURE OF THE ESSENTIAL PURPOSE OF ANY REMEDY.
 .
 C. Termination.
 .
 1. You may terminate this License at any time by notifying Sun in writing. 
 .
 2. All Your rights will terminate under this License if You fail to
 comply with any of its material terms or conditions and do not cure such
 failure within thirty (30) days after becoming aware of such
 noncompliance. 
 .
 3. Upon termination, You must discontinue all uses and distribution of
 the Technology, and all provisions of this Section V ("General Terms")
 shall survive termination.
 .
 D. Miscellaneous.
 .
 1. Trademark.  You agree to comply with Sun's Trademark & Logo Usage
 Requirements, as modified from time to time, available at
 http://www.sun.com/policies/trademarks/.  Except as expressly provided
 in this License, You are granted no rights in or to any Sun trademarks
 now or hereafter used or licensed by Sun. 
 .
 2. Integration.  This License represents the complete agreement of the
 parties concerning its subject matter.
 .
 3. Severability.  If any provision of this License is held
 unenforceable, such provision shall be reformed to the extent necessary
 to make it enforceable unless to do so would defeat the intent of the
 parties, in which case, this License shall terminate.
 .
 4. Governing Law.  This License is governed by the laws of the United
 States and the State of California, as applied to contracts entered into
 and performed in California between California residents. In no event
 shall this License be construed against the drafter.
 .
 5. Export Control.   As further described at http://www.sun.com/its, you
 agree to comply with the U.S. export controls and trade laws of other
 countries that apply to Technology and Modifications.

Files: src/share/mediaLib/linux/*/libmlib_jai.so
Copyright: Sun Microsystems, Inc.
License: MEDIALIB FOR JAI
 READ THE TERMS OF THIS AGREEMENT AND ANY PROVIDED SUPPLEMENTAL LICENSE
 TERMS (COLLECTIVELY "AGREEMENT") CAREFULLY BEFORE OPENING THE SOFTWARE
 MEDIA PACKAGE.  BY OPENING THE SOFTWARE MEDIA PACKAGE, YOU AGREE TO THE
 TERMS OF THIS AGREEMENT.  IF YOU ARE ACCESSING THE SOFTWARE
 ELECTRONICALLY, INDICATE YOUR ACCEPTANCE OF THESE TERMS BY SELECTING THE
 "ACCEPT" BUTTON AT THE END OF THIS AGREEMENT.  IF YOU DO NOT AGREE TO
 ALL THESE TERMS, PROMPTLY RETURN THE UNUSED SOFTWARE TO YOUR PLACE OF
 PURCHASE FOR A REFUND OR, IF THE SOFTWARE IS ACCESSED ELECTRONICALLY,
 SELECT THE "DECLINE" BUTTON AT THE END OF THIS AGREEMENT.
 .
 1. LICENSE TO USE. Sun grants you a non-exclusive and non-transferable
 license for the internal use only of the accompanying software and
 documentation,the Java Advanced  and any error corrections provided by
 Sun (collectively "Software"), by the number of users and the class of
 computer hardware for which the corresponding fee has been paid.
 .
 2. RESTRICTIONS. Software is confidential and copyrighted. Title to
 Software and all associated intellectual property rights is retained by
 Sun and/or its licensors.  Except as specifically authorized in any
 Supplemental License Terms, you may not make copies of Software, other
 than a single copy of Software for archival purposes.  Unless
 enforcement is prohibited by applicable law, you may not modify,
 decompile, or reverse engineer Software.  Licensee acknowledges that
 Software is not designed or intended for use in the design,
 construction, operation or maintenance of any nuclear facility. Sun
 Microsystems, Inc. disclaims any express or implied warranty of fitness
 for such uses.   No right, title or interest in or to any trademark,
 service mark, logo or trade name of Sun or its licensors is granted
 under this Agreement.
 .
 3.  LIMITED WARRANTY.  Sun warrants to you that for a period of ninety
 (90) days from the date of purchase, as evidenced by a copy of the
 receipt, the media on which Software is furnished (if any) will be free
 of defects in materials and workmanship under normal use. Except for the
 foregoing, Software is provided "AS IS".  Your exclusive remedy and
 Sun's entire liability under this limited warranty will be at Sun's
 option to replace Software media or refund the fee paid for Software.
 .
 4.  DISCLAIMER OF WARRANTY.  UNLESS SPECIFIED IN THIS AGREEMENT, ALL
 EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING
 ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE OR NON-INFRINGEMENT ARE DISCLAIMED, EXCEPT TO THE EXTENT THAT
 THESE DISCLAIMERS ARE HELD TO BE LEGALLY INVALID.
 .
 5.  LIMITATION OF LIABILITY.  TO THE EXTENT NOT PROHIBITED BY LAW, IN NO
 EVENT WILL SUN OR ITS LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT
 OR DATA, OR FOR SPECIAL, INDIRECT, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE
 DAMAGES, HOWEVER CAUSED REGARDLESS OF THE THEORY OF LIABILITY, ARISING
 OUT OF OR RELATED TO THE USE OF OR INABILITY TO USE SOFTWARE, EVEN IF
 SUN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  In no event
 will Sun's liability to you, whether in contract, tort (including
 negligence), or otherwise, exceed the amount paid by you for Software
 under this Agreement.  The foregoing limitations will apply even if the
 above stated warranty fails of its essential purpose.
 .
 6.  Termination.  This Agreement is effective until terminated.  You may
 terminate this Agreement at any time by destroying all copies of
 Software.  This Agreement will terminate immediately without notice from
 Sun if you fail to comply with any provision of this Agreement.  Upon
 Termination, you must destroy all copies of Software.
 .
 7.  Export Regulations. All Software and technical data delivered under
 this Agreement are subject to US export control laws and may be subject
 to export or import regulations in other countries.  You agree to comply
 strictly with all such laws and regulations and acknowledge that you
 have the responsibility to obtain such licenses to export, re-export, or
 import as may be required after delivery to you.
 .
 8.  U.S. Government Restricted Rights.  If Software is being acquired by
 or on behalf of the U.S. Government or by a U.S. Government prime
 contractor or subcontractor (at any tier), then the Government's rights
 in Software and accompanying documentation will be only as set forth in
 this Agreement; this is in accordance with 48 CFR 227.7201 through
 227.7202-4 (for Department of Defense (DOD) acquisitions) and with 48
 CFR 2.101 and 12.212 (for non-DOD acquisitions).
 .
 9.  Governing Law.  Any action related to this Agreement will be
 governed by California law and controlling U.S. federal law.  No choice
 of law rules of any jurisdiction will apply.
 .
 10. Severability. If any provision of this Agreement is held to be
 unenforceable, this Agreement will remain in effect with the provision
 omitted, unless omission would frustrate the intent of the parties, in
 which case this Agreement will immediately terminate.
 .
 11. Integration.  This Agreement is the entire agreement between you and
 Sun relating to its subject matter.  It supersedes all prior or
 contemporaneous oral or written communications, proposals,
 representations and warranties and prevails over any conflicting or
 additional terms of any quote, order, acknowledgment, or other
 communication between the parties relating to its subject matter during
 the term of this Agreement.  No modification of this Agreement will be
 binding, unless in writing and signed by an authorized representative of
 each party.
 .
 MEDIALIB FOR JAI
 .
 SUPPLEMENTAL LICENSE TERMS
 .
 These supplemental license terms ("Supplemental Terms") add to or modify
 the terms of the Binary Code License Agreement (collectively, the
 "Agreement"). Capitalized terms not defined in these Supplemental Terms
 shall have the same meanings ascribed to them in the Agreement. These
 Supplemental Terms shall supersede any inconsistent or conflicting terms
 in the Agreement, or in any license contained within the Software.
 .
 1. Software Internal Use and Development License Grant.  Subject to the
 terms and conditions of this Agreement, including, but not limited to
 Section 3 (Java Technology Restrictions) of these Supplemental Terms,
 Sun grants you a non-exclusive, non-transferable, limited license to
 reproduce internally and use internally the binary form of the Software
 and the Java Advanced Imaging and the JAI Image I/O Open Source Project,
 complete and unmodified, for the sole purpose of designing, developing
 and testing your Java applets and applications ("Programs").
 .
 2. License to Distribute Software.  In addition to the license granted
 in Section 1 (Software Internal Use and Development License Grant) of
 these Supplemental Terms, subject to the terms and conditions of this
 Agreement, including but not limited to, Section 3 (Java Technology
 Restrictions) of these Supplemental Terms, Sun grants you a
 non-exclusive, non-transferable, limited license to reproduce and
 distribute the Software in binary code form only, provided that you (i)
 distribute the Software complete and unmodified and only bundled as part
 of your Programs, (ii) do not distribute additional software intended to
 replace any component(s) of the Software, (iii) do not remove or alter
 any proprietary legends or notices contained in the Software, (iv) only
 distribute the Software subject to a license agreement that protects
 Sun's interests consistent with the terms contained in this Agreement,
 (v) agree to defend and indemnify Sun and its licensors from and against
 any damages, costs, liabilities, settlement amounts and/or expenses
 (including attorneys' fees) incurred in connection with any claim,
 lawsuit or action by any third party that arises or results from the use
 or distribution of any and all Programs and/or Software, and (vi) bundle
 the Software with the implementation of the Java Advanced Imaging and
 the JAI Image I/O Open Source Project.
 .
 3. Java Technology Restrictions. You may not modify the Java Platform
 Interface ("JPI", identified as classes contained within the "java"
 package or any subpackages of the "java" package), by creating
 additional classes within the JPI or otherwise causing the addition to
 or modification of the classes in the JPI.  In the event that you create
 an additional class and associated API(s) which (i) extends the
 functionality of the Java platform, and (ii) is exposed to third party
 software developers for the purpose of developing additional software
 which invokes such additional API, you must promptly publish broadly an
 accurate specification for such API for free use by all developers.  You
 may not create, or authorize your licensees to create additional
 classes, interfaces, or subpackages that are in any way identified as
 "java", "javax", "sun" or similar convention as specified by Sun in any
 naming convention designation.
 .
 4.  Java Runtime Availability.  Refer to the appropriate version of the
 Java Runtime Environment binary code license (currently located at
 http://www.java.sun.com/jdk/index.html) for the availability of runtime
 code which may be distributed with Java applets and applications.
 .
 5. Trademarks and Logos. You acknowledge and agree as between you and
 Sun that Sun owns the SUN, SOLARIS, JAVA, JINI, FORTE, and iPLANET
 trademarks and all SUN, SOLARIS, JAVA, JINI, FORTE, and iPLANET-related
 trademarks, service marks, logos and other brand designations ("Sun
 Marks"), and you agree to comply with the Sun Trademark and Logo Usage
 Requirements currently located at
 http://www.sun.com/policies/trademarks. Any use you make of the Sun
 Marks inures to Sun's benefit.
 .
 6. Source Code. Software may contain source code that is provided solely
 for reference purposes pursuant to the terms of this Agreement.  Source
 code may not be redistributed unless expressly provided for in this
 Agreement.
 .
 7. Termination for Infringement.  Either party may terminate this
 Agreement immediately should any Software become, or in either party's
 opinion be likely to become, the subject of a claim of infringement of
 any intellectual property right.

Files: debian/*
Copyright: © 2011 Mathieu Malaterre <malat@debian.org>
License: P-D
 Public domain
